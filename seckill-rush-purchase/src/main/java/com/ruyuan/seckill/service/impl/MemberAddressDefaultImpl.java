package com.ruyuan.seckill.service.impl;

import com.ruyuan.seckill.domain.MemberAddress;
import com.ruyuan.seckill.service.MemberAddressClient;
import com.ruyuan.seckill.service.MemberAddressManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 会员地址默认实现
 */
@Service
public class MemberAddressDefaultImpl implements MemberAddressClient {

    @Autowired
    private MemberAddressManager memberAddressManager;

    @Override
    public MemberAddress getModel(Integer id) {
        return memberAddressManager.getModel(id);
    }

    @Override
    public MemberAddress getDefaultAddress(Integer memberId) {
        return memberAddressManager.getDefaultAddress(memberId);
    }
}
