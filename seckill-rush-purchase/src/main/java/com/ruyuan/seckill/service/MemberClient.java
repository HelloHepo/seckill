package com.ruyuan.seckill.service;


import com.ruyuan.seckill.domain.Member;

/**
 * 会员客户户端
 */
public interface MemberClient {
    /**
     * 根据会员id获取会员信息
     *
     * @param memberId
     * @return
     */
    Member getModel(Integer memberId);


}
