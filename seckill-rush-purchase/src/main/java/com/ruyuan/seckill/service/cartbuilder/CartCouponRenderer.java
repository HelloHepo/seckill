package com.ruyuan.seckill.service.cartbuilder;

import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.vo.CartVO;
import com.ruyuan.seckill.domain.vo.CouponVO;

import java.util.List;

/**
 * 购物车优惠券渲染器
 */
public interface CartCouponRenderer {

    List<CouponVO> render(List<CartVO> cartList);
    List<CouponVO> render(List<CartVO> cartList, Buyer buyer);

}
