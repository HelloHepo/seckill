package com.ruyuan.seckill.domain.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.ruyuan.seckill.domain.Buyer;
import com.ruyuan.seckill.domain.dto.OrderDTO;
import com.ruyuan.seckill.domain.enums.CheckedWay;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;
import java.util.List;

/**
 * 交易VO
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class TradeVO implements Serializable {

    private static final long serialVersionUID = -8580648928412433120L;
    /**
     * 交易编号
     */
    private String tradeSn;
    /**
     * 会员id
     */
    private Integer memberId;
    /**
     * 会员昵称
     */
    private String memberName;
    /**
     * 支付方式
     */
    private String paymentType;
    /**
     * 价格信息
     */
    private PriceDetailVO priceDetail;
    /**
     * 收货人信息
     */
    private ConsigneeVO consignee;
    /**
     * 优惠券集合
     */
    private List<CouponVO> couponList;
    /**
     * 订单集合
     */
    private List<OrderDTO> orderList;

    @ApiModelProperty(value = "赠品集合")
    private List<GiftVO> giftList;

    @ApiModelProperty(value = "获取方式")
    private CheckedWay way;

    @ApiModelProperty(value = "会员")
    private Buyer buyer;
    /**
     * 消息状态，0代表已发送，1代表接收成功，2代表消费失败
     * 默认就是已发送
     */
    private Integer messageStatus = 0;

    public Integer getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(Integer messageStatus) {
        this.messageStatus = messageStatus;
    }

    public List<GiftVO> getGiftList() {
        return giftList;
    }

    public void setGiftList(List<GiftVO> giftList) {
        this.giftList = giftList;
    }

    public CheckedWay getWay() {
        return way;
    }

    public void setWay(CheckedWay way) {
        this.way = way;
    }

    public Buyer getBuyer() {
        return buyer;
    }

    public void setBuyer(Buyer buyer) {
        this.buyer = buyer;
    }

    public String getTradeSn() {
        return tradeSn;
    }

    public void setTradeSn(String tradeSn) {
        this.tradeSn = tradeSn;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public PriceDetailVO getPriceDetail() {
        return priceDetail;
    }

    public void setPriceDetail(PriceDetailVO priceDetail) {
        this.priceDetail = priceDetail;
    }

    public ConsigneeVO getConsignee() {
        return consignee;
    }

    public void setConsignee(ConsigneeVO consignee) {
        this.consignee = consignee;
    }

    public List<CouponVO> getCouponList() {
        return couponList;
    }

    public void setCouponList(List<CouponVO> couponList) {
        this.couponList = couponList;
    }

    public List<OrderDTO> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<OrderDTO> orderList) {
        this.orderList = orderList;
    }


}
