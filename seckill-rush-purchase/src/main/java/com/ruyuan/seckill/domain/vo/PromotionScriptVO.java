package com.ruyuan.seckill.domain.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 促销脚本VO
 **/
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PromotionScriptVO implements Serializable {
    private static final long serialVersionUID = 3566902764098210013L;

    @ApiModelProperty(value = "促销活动id")
    private Integer promotionId;

    @ApiModelProperty(value = "促销活动名称")
    private String promotionName;

    @ApiModelProperty(value = "促销活动类型")
    private String promotionType;

    @ApiModelProperty(value = "是否可以被分组")
    private Boolean isGrouped;

    @ApiModelProperty(value = "促销脚本", hidden = true)
    private String promotionScript;

    @ApiModelProperty(value = "商品skuID")
    private Integer skuId;


    public Integer getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(Integer promotionId) {
        this.promotionId = promotionId;
    }

    public String getPromotionName() {
        return promotionName;
    }

    public void setPromotionName(String promotionName) {
        this.promotionName = promotionName;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public Boolean getIsGrouped() {
        return isGrouped;
    }

    public void setIsGrouped(Boolean grouped) {
        isGrouped = grouped;
    }

    public String getPromotionScript() {
        return promotionScript;
    }

    public void setPromotionScript(String promotionScript) {
        this.promotionScript = promotionScript;
    }

    public Integer getSkuId() {
        return skuId;
    }

    public void setSkuId(Integer skuId) {
        this.skuId = skuId;
    }


}
